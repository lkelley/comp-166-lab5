//
// Created by Landon on 1/17/2020.
//
#include <math.h>

#include "statistics.h"

double mean(const double sum, const int count) {
    return sum / count;
}

double ssdev(const double sum, const double sumsq, const int count) {
    return sqrt((count * sumsq - pow(sum,2)) / (count * (count - 1)));
}
