//
// Created by Landon on 1/17/2020.
//

#ifndef LAB_5___STATISTICS_AND_FILES_STATISTICS_H
#define LAB_5___STATISTICS_AND_FILES_STATISTICS_H

/**
 * Computes the mean value from a sum and count
 * @param sum The sum of the values
 * @param count The number of values
 * @return The mean value
 */
double mean(const double sum, const int count);


/**
 * Computes the standard deviation from the sum, the sum of the squares, and the count
 * @param sum Sum of all values
 * @param sumsq Sum of all values, individually squared
 * @param count The number of values
 * @return The standard deviation
 */
double ssdev(const double sum, const double sumsq, const int count);

#endif //LAB_5___STATISTICS_AND_FILES_STATISTICS_H
