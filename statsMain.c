#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <libgen.h>

#include "statistics.h"

int main(int cnt, char *argv[]) {
    // Check argument count
    if (cnt != 2) {
        printf("Correct usage: statsMain dataFileName\n");
        return EXIT_FAILURE;
    }


    // Declare file pointers
    FILE *dataFile;
    FILE *resultsFile;

    // Declare stats vars
    double sum = 0;
    double sumsq = 0;
    int count = 0;
    double meanValue;
    double standardDev;


    /*
     * Attempt to open the data file
     */
    if (!(dataFile = fopen(argv[1], "r"))) {
        printf("Unable to open input file \"%s\"", argv[1]);
        return EXIT_FAILURE;
    }


    /*
     * Determine the results file name
     *
     * argv[1] contains the passed path to the data file, which may be an absolute path.
     * If it is, the path components must be stripped to get the basename.
     */
    char *dataFileName = basename(argv[1]); // Get the basename of the data file
    char *resultsFileName = (char *)malloc(sizeof("Result_") + strlen(dataFileName)); // Allocate memory for combined string
    strcpy(resultsFileName, "Result_"); // Put Result_ at the beginning of the string
    strcat(resultsFileName, dataFileName); // Add the filename to the string

    // Open the results file
    if (!(resultsFile = fopen(resultsFileName, "w"))) {
        printf("Unable to open output file \"%s\"", resultsFileName);
        fclose(dataFile);
        return EXIT_FAILURE;
    }


    /*
     * Declare placeholder vars for file reading.
     */
    char sValue[1024]; // Buffer for reading input value
    double dValue; // Temp var for converted double from string

    /*
     * Walk input file, reading strings that should be numbers
     */
    while (fscanf(dataFile, "%1023s", sValue) == 1) {
        dValue = strtod(sValue, NULL); // Convert to double
        sum += dValue; // Add to sum
        sumsq += pow(dValue, 2); // Add to squared sum
        count++; // Increment count
    }

    // Close the data file
    fclose(dataFile);


    // Check for min value count
    if (count < 2) {
        printf("File must have at least 2 values");
        fclose(resultsFile);
        return EXIT_FAILURE;
    }


    /*
     * Calculate stats vars
     */
    meanValue = mean(sum, count);
    standardDev = ssdev(sum, sumsq, count);


    // Declare output string, allocate memory
    char *output = (char *)malloc(sizeof(char) * 1024);

    // Format output string
    sprintf(output, "%d Values, Mean = %lf, Sample Standard Deviation = %lf", count, meanValue, standardDev);

    // Print output string to console
    puts(output);

    // Print output string to file
    fputs(output, resultsFile);

    // Close output file
    fclose(resultsFile);

    return EXIT_SUCCESS;
}
